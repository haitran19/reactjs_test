import React from 'react';
import { Row, Col } from 'antd';

import styles from './index.less';

const Social: React.FC = () => {
  return (
    <Row className={styles.container}>
      <Col span={24}>
        <a className={styles.loginGoogle} href="#" title="Login with google">
          Continue with Google
        </a>
      </Col>
      <Col span={24}>
        <a className={styles.loginFacebook} href="#" title="Login with facebook">
          Continue with Facebook
        </a>
      </Col>
    </Row>
  );
};

export default Social;

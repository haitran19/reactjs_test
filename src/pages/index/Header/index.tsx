import React from 'react';
import { Row, Col } from 'antd';
import styles from './index.less';

const Header: React.FC = () => {
  return (
    <Row justify="center" className={styles.container}>
      <Col>
        <p className={styles.title}>
          Welcome back <br />
          to the<span className={styles.highLight}>*</span>Game
          <span className={styles.highLight2}>her</span>'s <br />
          connect !
        </p>
      </Col>
    </Row>
  );
};

export default Header;

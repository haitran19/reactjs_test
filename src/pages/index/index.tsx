import React from 'react';
import { Row, Col, Input, Checkbox, Form, Button } from 'antd';
import Header from './Header';
import Social from './Social';
import styles from './index.less';

export default function Index() {
  return (
    <Row justify="center" align="middle" className={styles.container}>
      <Col>
        <Header />
        <Form className={styles.form}>
          <Form.Item>
            <Input prefix={<div className={styles.userLogo} />} placeholder="Username or Email" />
          </Form.Item>
          <Form.Item>
            <Input
              prefix={<div className={styles.passwordLogo} />}
              suffix={
                <a title="Forgot password" href="#" className={styles.passwordForgot}>
                  Forgot password?
                </a>
              }
              type="password"
              placeholder="Password"
            />
          </Form.Item>
          <Form.Item>
            <Checkbox className={styles.remember}>Remember me</Checkbox>
          </Form.Item>
          <Form.Item>
            <Button htmlType="submit" className={styles.btnSubmit}>
              Login
            </Button>
          </Form.Item>
        </Form>
        <Social />
        <Row className={styles.boxAction}>
          <p>
            Don't have an account?{' '}
            <a href="#" title="Sign up" className={styles.signup}>
              Sign Up
            </a>
          </p>
        </Row>
      </Col>
    </Row>
  );
}
